package tictactoe.ilovesyntax.com.authsample;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseAuthRecentLoginRequiredException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;

import butterknife.Bind;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.tv_error_should_be)
    TextView tvErrorShouldBe;

    @Bind(R.id.tv_error_is)
    TextView tvErrorIs;

    @Bind(R.id.container)
    LinearLayout container;

    FirebaseUser user = null;

    ProgressDialog progressDialog;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        ButterKnife.bind(this);

        FirebaseAuth.getInstance().addAuthStateListener(new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = firebaseAuth.getCurrentUser();

                mAuth = FirebaseAuth.getInstance();

                container.setVisibility(user == null ? View.GONE : View.VISIBLE);
            }
        });
    }

    @OnClick({R.id.btn_sign_in,
            R.id.btn_change_invalid,
            R.id.btn_change_exisiting,
            R.id.btn_reauthenticate,
            R.id.btn_sign_out})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_sign_in:
//                if (user != null) {
//                    promptUser("Action disabled");
//                    return;
//                }
                showDialog();
                mAuth.signInWithEmailAndPassword("test@gmail.com", "password")
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                dismissDialog();
                                if (e instanceof FirebaseAuthInvalidUserException) {
                                    promptUser("Invalid User");
                                } else if (e instanceof FirebaseAuthInvalidCredentialsException) {
                                    promptUser("Wrong password");
                                } else {
                                    promptUser(e.getMessage());
                                }
                            }
                        })
                        .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                            @Override
                            public void onSuccess(AuthResult authResult) {
                                dismissDialog();
                                promptUser("Signed in");
                            }
                        });
                break;

            case R.id.btn_change_invalid:
                if (user == null) {
                    promptUser("Action disabled");
                    return;
                }
                showDialog();

                tvErrorShouldBe.setText(FirebaseAuthInvalidCredentialsException.class.getSimpleName());
                user.updateEmail("?? ??@gmail.com")
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                dismissDialog();
                                tvErrorIs.setText(e.getClass().getSimpleName());
                                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                                    promptUser("Email is malformed");
                                } else if (e instanceof FirebaseAuthUserCollisionException) {
                                    promptUser("Account already exists");
                                } else if (e instanceof FirebaseAuthInvalidUserException) {
                                    promptUser("Account disabled");
                                } else if (e instanceof FirebaseAuthRecentLoginRequiredException) {
                                    tvErrorShouldBe.setText(e.getClass().getSimpleName());
                                    promptUser("You have to reauthenticate");
                                } else {
                                    promptUser("BUG - Generic FirebaseException ");
                                }
                            }
                        })
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                dismissDialog();
                                promptUser("Email changed");
                            }
                        });
                break;

            case R.id.btn_change_exisiting:
                if (user == null) {
                    promptUser("Action disabled");
                    return;
                }
                showDialog();
                tvErrorShouldBe.setText(FirebaseAuthUserCollisionException.class.getSimpleName());
                user.updateEmail("existing@gmail.com")
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                dismissDialog();
                                tvErrorIs.setText(e.getClass().getSimpleName());
                                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                                    promptUser("Email is malformed");
                                } else if (e instanceof FirebaseAuthUserCollisionException) {
                                    promptUser("Account already exists");
                                } else if (e instanceof FirebaseAuthInvalidUserException) {
                                    promptUser("Account disabled");
                                } else if (e instanceof FirebaseAuthRecentLoginRequiredException) {
                                    tvErrorShouldBe.setText(e.getClass().getSimpleName());
                                    promptUser("You have to reauthenticate");
                                } else {
                                    promptUser("BUG - Generic FirebaseException");
                                }
                            }
                        })
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                dismissDialog();
                                promptUser("Email changed");
                            }
                        });
                break;

            case R.id.btn_reauthenticate:
                if (user == null) {
                    promptUser("Action disabled");
                    return;
                }
                showDialog();
                AuthCredential credential = EmailAuthProvider.getCredential("test@gmail.com", "password");
                user.reauthenticate(credential)
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                dismissDialog();
                                if (e instanceof FirebaseAuthInvalidUserException) {
                                    promptUser("Account disabled");
                                } else if (e instanceof FirebaseAuthInvalidCredentialsException) {
                                    promptUser("Credential malformed");
                                } else {
                                    promptUser(e.getMessage());
                                }
                            }
                        })
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                dismissDialog();
                                promptUser("Reauthenticated");
                            }
                        });
                break;

            case R.id.btn_sign_out:
                if (user == null) {
                    promptUser("Action disabled");
                    return;
                }
                tvErrorIs.setText("");
                tvErrorShouldBe.setText("");
                FirebaseAuth.getInstance().signOut();
                break;
        }
    }

    private void showDialog() {
        dismissDialog();
        progressDialog = ProgressDialog.show(this, "", "Loading", true);
    }

    private void dismissDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    private void promptUser(String message) {
        Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
    }
}
